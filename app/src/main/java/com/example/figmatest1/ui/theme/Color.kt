package com.example.figmatest1.ui.theme

import androidx.compose.ui.graphics.Color

val Blue = Color(0xFF0560FA)
val TextGray1 = Color(0xFF3A3A3A)
val DotGray = Color(0xFFA7A7A7)