package com.example.figmatest1.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

sealed interface WelcomeUiState {
    object Splash: WelcomeUiState
    object FirstScreen: WelcomeUiState
    object TwoScreen: WelcomeUiState
    object ThreeScreen: WelcomeUiState
}

class WelcomeViewModel(): ViewModel() {

    private val _screenUiState: MutableStateFlow<WelcomeUiState> = MutableStateFlow(WelcomeUiState.Splash)
    val screenUiState: StateFlow<WelcomeUiState> = _screenUiState.asStateFlow()

    init {
        loadSplash()
    }

    private fun loadSplash() {
        viewModelScope.launch {
            _screenUiState.value = WelcomeUiState.Splash
            delay(2000)
            _screenUiState.value = WelcomeUiState.FirstScreen
        }
    }

    fun clickNext() {
        if (screenUiState.value == WelcomeUiState.FirstScreen) {
            _screenUiState.value = WelcomeUiState.TwoScreen
        } else if (screenUiState.value == WelcomeUiState.TwoScreen) {
            _screenUiState.value = WelcomeUiState.ThreeScreen
        }
    }
}