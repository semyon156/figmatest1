package com.example.figmatest1.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.postgrest.from
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable

@Serializable
data class User(
    val id: Int? = null,
    val name: String,
    val password: String
)

val supabase = createSupabaseClient(
    supabaseUrl = "https://xxfybzlohqrjvckttvis.supabase.co",
    supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inh4ZnliemxvaHFyanZja3R0dmlzIiwicm9sZSI6InNlcnZpY2Vfcm9sZSIsImlhdCI6MTcwNjUwNzkyMCwiZXhwIjoyMDIyMDgzOTIwfQ.4b0AHTLuTjsMZJ0sfuGBoYIcFj4Y_25q20sKggsZYno"
) {
    install(Postgrest)
}

class SupabaseViewModel() : ViewModel() {
    private val _users: MutableStateFlow<List<User>> = MutableStateFlow(listOf())
    val users: StateFlow<List<User>> = _users.asStateFlow()

    private val _nameText: MutableStateFlow<String> = MutableStateFlow("")
    val nameText: StateFlow<String> = _nameText.asStateFlow()

    private val _passText: MutableStateFlow<String> = MutableStateFlow("")
    val passText: StateFlow<String> = _passText.asStateFlow()

    fun updateNameText(newValue: String) {
        _nameText.value = newValue
    }

    fun updatePassText(newValue: String) {
        _passText.value = newValue
    }

    fun addUser() {
        viewModelScope.launch(Dispatchers.IO) {
            supabase.from("users").insert(User(name = nameText.value, password = passText.value))
        }
    }

    fun updateUser() {
        viewModelScope.launch(Dispatchers.IO) {
            supabase.from("users")
                .update({
                    set("name", "seven")
                    set("password", "7")
                }) { filter { eq("id", 7) } }
        }
    }

    init {
        loadUsers()

    }

    private fun loadUsers() {
        viewModelScope.launch(Dispatchers.IO) {
            _users.value = supabase.from("users").select().decodeList<User>()
        }
    }
}