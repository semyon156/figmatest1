package com.example.figmatest1

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.figmatest1.ui.theme.FigmaTest1Theme
import com.example.figmatest1.viewmodels.SupabaseViewModel
import com.example.figmatest1.viewmodels.WelcomeUiState
import com.example.figmatest1.viewmodels.WelcomeViewModel
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.postgrest.from
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FigmaTest1Theme {
                ViewTest()
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ViewTest() {
    Scaffold(bottomBar = { BottomBar() }) {
        Box(modifier = Modifier.padding(it)) {

        }
    }
}

@Composable
fun BottomBar() {
    var selected by rememberSaveable { mutableStateOf(0) }

    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(20.dp)
                .background(
                    brush = Brush.verticalGradient(
                        colors = listOf(
                            Color.Transparent,
                            Color.Gray.copy(alpha = 0.1f)
                        )
                    )
                )
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(60.dp)
                .padding(horizontal = 14.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            BottomBarItem(
                icon = R.drawable.bb1,
                text = "Home",
                selected == 0,
                selectedIcon = R.drawable.bb1_selected,
                click = { selected = 0 })
            BottomBarItem(
                icon = R.drawable.bb2,
                text = "Wallet",
                selected == 1,
                selectedIcon = R.drawable.bb2_selected,
                click = { selected = 1 })
            BottomBarItem(
                icon = R.drawable.bb3,
                text = "Track",
                selected == 2,
                selectedIcon = R.drawable.bb3_selected,
                click = { selected = 2 })
            BottomBarItem(
                icon = R.drawable.bb4,
                text = "Profile",
                selected == 3,
                selectedIcon = R.drawable.bb4_selected,
                click = { selected = 3 })
        }
    }
}

@Composable
fun BottomBarItem(
    icon: Int,
    text: String,
    isSelected: Boolean,
    selectedIcon: Int,
    click: () -> Unit
) {
    Box(modifier = Modifier
        .fillMaxHeight()
        .width(35.dp),
        contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier.clickable { click() },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(
                painterResource(id = if (isSelected) selectedIcon else icon),
                contentDescription = "bb_icon",
                tint = Color.Unspecified
            )
            Text(
                text = text,
                color = if (isSelected) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.tertiary,
                fontSize = 10.sp
            )
        }
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.TopCenter) {
            Column {
                if (isSelected) {
                    Box(
                        modifier = Modifier
                            .size(height = 2.dp, width = 35.dp)
                            .clip(RoundedCornerShape(20.dp))
                            .background(MaterialTheme.colorScheme.primary)
                    )
                    Box(
                        modifier = Modifier
                            .size(height = 2.dp, width = 35.dp)
                            .clip(RoundedCornerShape(20.dp))
                            .background(
                                brush = Brush.verticalGradient(
                                    colors = listOf(
                                        MaterialTheme.colorScheme.primary.copy(alpha = 0.4f),
                                        Color.Transparent
                                    )
                                )
                            )
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Test(vm: SupabaseViewModel = viewModel()) {
    val users = vm.users.collectAsState()
    val nameText = vm.nameText.collectAsState()
    val passText = vm.passText.collectAsState()

    Column {
        TextField(value = nameText.value, onValueChange = { vm.updateNameText(it) })
        TextField(value = passText.value, onValueChange = { vm.updatePassText(it) })
        Button(onClick = { vm.addUser() }) {
            Text(text = "Add user")
        }
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            items(users.value) { user ->
                Card {
                    Column {
                        Text(text = user.name)
                        Text(text = user.password)
                    }
                }
            }
        }
    }


}

@Composable
fun SplashScreen() {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Image(
            painter = painterResource(id = R.drawable.splash),
            contentDescription = "SplashScreen"
        )
    }
}

@Composable
fun WelcomeDot(color: Color) {
    Box(
        Modifier
            .size(8.4.dp)
            .background(color = color, shape = RoundedCornerShape(size = 8.4.dp))
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun WelcomeFirstScreen(clickNext: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .padding(horizontal = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.size(48.dp))
        Image(
            painter = painterResource(id = R.drawable.in_no_time_pana_1),
            contentDescription = "in_no_time_pana_1"
        )
        Spacer(modifier = Modifier.size(48.dp))
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(
                text = "Quick Delivery At Your Doorstep",
                style = TextStyle(
                    fontSize = 24.sp,
                    lineHeight = 24.sp,
                    fontWeight = FontWeight(700),
                    color = MaterialTheme.colorScheme.primary,
                    textAlign = TextAlign.Center,
                ),
                modifier = Modifier.padding(horizontal = 20.dp)
            )
            Spacer(modifier = Modifier.size(10.dp))
            Text(
                text = "Enjoy quick pick-up and delivery to your destination",
                style = TextStyle(
                    fontSize = 16.sp,
                    lineHeight = 20.sp,
                    fontWeight = FontWeight(400),
                    color = MaterialTheme.colorScheme.secondary,
                    textAlign = TextAlign.Center,
                ),
                modifier = Modifier.padding(horizontal = 50.dp)
            )
            Spacer(modifier = Modifier.size(43.dp))
            Row {
                WelcomeDot(color = MaterialTheme.colorScheme.primary)
                Spacer(modifier = Modifier.size(8.4.dp))
                WelcomeDot(color = MaterialTheme.colorScheme.tertiary)
                Spacer(modifier = Modifier.size(8.4.dp))
                WelcomeDot(color = MaterialTheme.colorScheme.tertiary)
            }
            Spacer(modifier = Modifier.size(82.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Card(
                    modifier = Modifier
                        .border(
                            shape = RoundedCornerShape(size = 4.69226.dp),
                            width = 1.dp,
                            color = MaterialTheme.colorScheme.primary
                        )
                        .width(55.36497.dp)
                        .height(28.76902.dp),
                    shape = RoundedCornerShape(size = 4.69226.dp),
                    colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.background),
                    onClick = { }
                ) {
                    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                        Text(
                            text = "Skip",
                            style = TextStyle(
                                fontSize = 9.38.sp,
                                fontWeight = FontWeight(700),
                                color = MaterialTheme.colorScheme.primary,
                            )
                        )
                    }
                }
                Card(
                    modifier = Modifier
                        .width(56.36497.dp)
                        .height(28.76902.dp),
                    shape = RoundedCornerShape(size = 4.69226.dp),
                    colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.primary),
                    onClick = { clickNext() }
                ) {
                    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                        Text(
                            text = "Next",
                            style = TextStyle(
                                fontSize = 9.38.sp,
                                fontWeight = FontWeight(700),
                                color = MaterialTheme.colorScheme.background,
                            )
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.size(20.dp))
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun WelcomeTwoScreen(clickNext: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .padding(horizontal = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.size(48.dp))
        Image(
            painter = painterResource(id = R.drawable.rafiki),
            contentDescription = "rafiki"
        )
        Spacer(modifier = Modifier.size(48.dp))
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(
                text = "Flexible Payment",
                style = TextStyle(
                    fontSize = 24.sp,
                    lineHeight = 24.sp,
                    fontWeight = FontWeight(700),
                    color = MaterialTheme.colorScheme.primary,
                    textAlign = TextAlign.Center,
                ),
                modifier = Modifier.padding(horizontal = 20.dp)
            )
            Spacer(modifier = Modifier.size(10.dp))
            Text(
                text = "Different modes of payment either before and after delivery without stress",
                style = TextStyle(
                    fontSize = 16.sp,
                    lineHeight = 20.sp,
                    fontWeight = FontWeight(400),
                    color = MaterialTheme.colorScheme.secondary,
                    textAlign = TextAlign.Center,
                ),
                modifier = Modifier.padding(horizontal = 50.dp)
            )
            Spacer(modifier = Modifier.size(43.dp))
            Row {
                WelcomeDot(color = MaterialTheme.colorScheme.tertiary)
                Spacer(modifier = Modifier.size(8.4.dp))
                WelcomeDot(color = MaterialTheme.colorScheme.primary)
                Spacer(modifier = Modifier.size(8.4.dp))
                WelcomeDot(color = MaterialTheme.colorScheme.tertiary)
            }
            Spacer(modifier = Modifier.size(82.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Card(
                    modifier = Modifier
                        .border(
                            shape = RoundedCornerShape(size = 4.69226.dp),
                            width = 1.dp,
                            color = MaterialTheme.colorScheme.primary
                        )
                        .width(55.36497.dp)
                        .height(28.76902.dp),
                    shape = RoundedCornerShape(size = 4.69226.dp),
                    colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.background),
                    onClick = { }
                ) {
                    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                        Text(
                            text = "Skip",
                            style = TextStyle(
                                fontSize = 9.38.sp,
                                fontWeight = FontWeight(700),
                                color = MaterialTheme.colorScheme.primary,
                            )
                        )
                    }
                }
                Card(
                    modifier = Modifier
                        .width(56.36497.dp)
                        .height(28.76902.dp),
                    shape = RoundedCornerShape(size = 4.69226.dp),
                    colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.primary),
                    onClick = { clickNext() }
                ) {
                    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                        Text(
                            text = "Next",
                            style = TextStyle(
                                fontSize = 9.38.sp,
                                fontWeight = FontWeight(700),
                                color = MaterialTheme.colorScheme.background,
                            )
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.size(20.dp))
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun WelcomeThreeScreen() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .padding(horizontal = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.size(48.dp))
        Image(
            painter = painterResource(id = R.drawable.rafiki2),
            contentDescription = "rafiki2"
        )
        Spacer(modifier = Modifier.size(48.dp))
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(
                text = "Real-time Tracking",
                style = TextStyle(
                    fontSize = 24.sp,
                    lineHeight = 24.sp,
                    fontWeight = FontWeight(700),
                    color = MaterialTheme.colorScheme.primary,
                    textAlign = TextAlign.Center,
                ),
                modifier = Modifier.padding(horizontal = 20.dp)
            )
            Spacer(modifier = Modifier.size(10.dp))
            Text(
                text = "Track your packages/items from the comfort of your home till final destination",
                style = TextStyle(
                    fontSize = 16.sp,
                    lineHeight = 20.sp,
                    fontWeight = FontWeight(400),
                    color = MaterialTheme.colorScheme.secondary,
                    textAlign = TextAlign.Center,
                ),
                modifier = Modifier.padding(horizontal = 50.dp)
            )
            Spacer(modifier = Modifier.size(43.dp))
            Row {
                WelcomeDot(color = MaterialTheme.colorScheme.tertiary)
                Spacer(modifier = Modifier.size(8.4.dp))
                WelcomeDot(color = MaterialTheme.colorScheme.tertiary)
                Spacer(modifier = Modifier.size(8.4.dp))
                WelcomeDot(color = MaterialTheme.colorScheme.primary)
            }
            Spacer(modifier = Modifier.size(62.6.dp))
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Card(
                    modifier = Modifier
                        .height(46.dp)
                        .fillMaxWidth(),
                    shape = RoundedCornerShape(size = 4.dp),
                    colors = CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.primary),
                    onClick = { }
                ) {
                    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                        Text(
                            text = "Sign Up",
                            style = TextStyle(
                                fontSize = 16.sp,
                                lineHeight = 16.sp,
                                fontWeight = FontWeight(700),
                                color = MaterialTheme.colorScheme.background,
                                textAlign = TextAlign.Center,
                            )
                        )
                    }
                }
                Spacer(modifier = Modifier.size(10.dp))
                Row {
                    Text(
                        text = "Already have an account?",
                        style = TextStyle(
                            fontSize = 14.sp,
                            lineHeight = 16.sp,
                            fontWeight = FontWeight(400),
                            color = MaterialTheme.colorScheme.tertiary,
                        )
                    )
                    Text(
                        text = "Sign in",
                        style = TextStyle(
                            fontSize = 14.sp,
                            lineHeight = 16.sp,
                            fontWeight = FontWeight(500),
                            color = MaterialTheme.colorScheme.primary,
                        )
                    )
                }
            }
            Spacer(modifier = Modifier.size(20.dp))
        }
    }
}

@Composable
fun Welcome(vm: WelcomeViewModel = viewModel()) {
    val screenUiState = vm.screenUiState.collectAsState()

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
    ) {
        when (screenUiState.value) {
            WelcomeUiState.Splash -> {
                SplashScreen()
            }

            WelcomeUiState.FirstScreen -> {
                WelcomeFirstScreen { vm.clickNext() }
            }

            WelcomeUiState.TwoScreen -> {
                WelcomeTwoScreen { vm.clickNext() }
            }

            else -> {
                WelcomeThreeScreen()
            }
        }
    }
}